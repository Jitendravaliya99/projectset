// popover start
$(document).ready(function () {

    $(".divs div").each(function (e) {
        if (e != 0)
            $(this).hide();
    });

    $("#next").click(function () {
        if ($(".divs div:visible").next().length != 0) {
            $(".divs div:visible").next().show().prev().hide();
            // $(this).css("transform","translateX("+$(this).index() * -450+"px)");
        } else {
            $(".divs div:visible").hide();
            $(".divs div:first").show();
        }
        return false;
    });

    $("#prev").click(function () {
        if ($(".divs div:visible").prev().length != 0)
            $(".divs div:visible").prev().show().next().hide();
        else {
            $(".divs div:visible").hide();
            $(".divs div:last").show();
        }
        return false;
    });
    $('.input-carousel').popover({
        html: true,
        trigger: "click",
        placement: "bottom",
        title: ' <a href="#" class="close" data-dismiss="alert">&times;</a>'
    });
});
$(document).on("click", ".popover .close", function () {
    $(this).parents(".popover").popover('hide');
});

$('body').on('click', function (e) {
    $('[data-toggle=popover]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e
                .target).length === 0) {
            $(this).popover('hide');
        }
    });
});

$('input[type="radio"][name="action"]').on('change', function () {
    var ThisIt = $(this);
    if (ThisIt.val() == "yes") {
        // when user select yes

        $('#show-question').fadeIn();
        $('#show-question').find("input").val("");
        $('#show-question').find('select option:first').prop('selected', true);
    } else {
        // when user select no
        $('#show-question').fadeOut();
    }
});
// popover end