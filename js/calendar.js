$(document).ready(function () {
    $('.js-example-basic-single').select2({
        "language": {
            "noResults": function () {
                return "No Results Found <a href='#' class='link'>Create an Invite</a>";
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }

    });
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        defaultDate: '2020-09-12',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [{
                title: 'Planning',
                start: '2020-09-17',
                backgroundColor: '#FFE2BF',
                borderColor: 'transparent'
            },
            {
                title: 'Long Event',
                start: '2020-09-19',
                end: '2020-09-10'
            },
            {
                groupId: 999,
                title: 'Repeating Event',
                start: '2020-09-09T16:00:00'
            },
            {
                groupId: 999,
                title: 'Repeating Event',
                start: '2020-09-16T16:00:00'
            },
            {
                title: 'Conference',
                start: '2020-09-11',
                end: '2020-09-13'
            },
            {
                title: 'Meeting',
                start: '2020-09-12T10:30:00',
                end: '2020-09-12T12:30:00'
            },
            {
                title: 'Lunch',
                start: '2020-09-12T12:00:00'
            },
            {
                title: 'Meeting',
                start: '2020-09-12T14:30:00'
            },
            {
                title: 'Happy Hour',
                start: '2020-09-12T17:30:00'
            },
            {
                title: 'Dinner',
                start: '2020-09-12T20:00:00'
            },
            {
                title: 'Birthday Party',
                start: '2020-09-13 T 07:00:00'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2020-09-28'
            }
        ]
    });

    $(".fc-header-toolbar").append(
        '<select class="select_month" selected><option value="">Select Month</option><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">Augest</option><option value="9" >September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></select>'
    );

    $(".select_month").on("change", function (event) {
        $('#calendar').fullCalendar('changeView', 'month', this.value);
        $('#calendar').fullCalendar('gotoDate', "2020-" + this.value + "-1");
    });
    $(".select_month").select2();

});