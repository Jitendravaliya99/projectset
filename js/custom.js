// carousels
$(document).ready(function () {

    // slick slider - employer page
    $('.common-carousel-slick').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 6,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });

    // slick slider - employer page
    $('.common-carousel-slick').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 6,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });

    $('.owl-recommend').owlCarousel({
        loop: false,
        margin: 24,
        nav: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
                dots: true,
                margin: 16,
            },
            600: {
                items: 1,
                nav: false,
                dots: true,
                margin: 10,
            },
            601: {
                items: 2,
                nav: false,
                loop: false,
                dots: false
            },
            991: {
                items: 3,
                nav: false,
                loop: false,
                dots: false
            },
            1000: {
                items: 3,
                nav: false,
                loop: false,
                dots: false
            }
        }
    })
});

// custom accordion start

// filter overflow hiddon on outside click
var mouse_is_inside = false;

$(document).ready(function () {
    $('.filter-box').hover(function () {
        mouse_is_inside = true;
    }, function () {
        mouse_is_inside = false;
    });

    $("body").mouseup(function () {
        if (!mouse_is_inside) $('#filterBoxContentone').hide();
        $('.filter-box__btn').removeClass('filter-box__btn-down-active');
    });

    $("body").mouseup(function () {
        if (!mouse_is_inside) $('#filterBoxContenttwo').hide();
        $('.filter-box__btn-2').removeClass('filter-box__btn-2-down-active');
    });

    // $("body").mouseup(function () {
    //     if (!mouse_is_inside) $('#filterBoxContenttwo').hide();
    //     $('.filter-box__btn').removeClass('filter-box__btn-down-active');
    // });

    // $("body").mouseup(function () {
    //     if (!mouse_is_inside) $('#filterContent').hide();
    //     $('.filter-box__deliverable-btn').removeClass('filter-box__deliverable-btn-down-active');
    // });

    // $("body").mouseup(function () {
    //     if (!mouse_is_inside) $('#filterContenttwo').hide();
    //     $('.filter-box__deliverable-btn').removeClass('filter-box__deliverable-btn-down-active');
    // });
});

// accordion for filter box
var acc = document.getElementsByClassName("custom-accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}

// filter box one
function myFunctionone() {
    var x = document.getElementById("filterBoxContentone");
    if (x.style.display === "none") {
        x.style.display = "block";
        $('.filter-box__btn').addClass('filter-box__btn-down-active');
    } else {
        x.style.display = "none";
        $('.filter-box__btn').removeClass('filter-box__btn-down-active');
    }
}

// filter box two
function myFunctiontwo() {
    var x = document.getElementById("filterBoxContenttwo");
    if (x.style.display === "none") {
        x.style.display = "block";
        $('.filter-box__btn-2').addClass('filter-box__btn-2-down-active');
    } else {
        x.style.display = "none";
        $('.filter-box__btn-2').removeClass('filter-box__btn-2-down-active');
    }
}

// filter box btn
function myFunctionbtn() {
    var x = document.getElementById("filterBoxContentbtn");
    if (x.style.display === "none") {
        x.style.display = "block";
        $('.filter-box__style-2-btn').addClass('filter-box__style-2-btn-down-active');
    } else {
        x.style.display = "none";
        $('.filter-box__style-2-btn').removeClass('filter-box__style-2-btn-down-active');
    }
}

// input checked add checked icon on the filter dropdown
$('.checkbox-input-box__input:checkbox').change(function () {
    if ($(this).is(":checked")) {
        $('.filter-box__btn').addClass("filter-box__btn-add");
    } else {
        $('.filter-box__btn').removeClass("filter-box__btn-add");
    }
});
// custom accordion ends

$(document).ready(function () {
    // ckeditor
    $('.content').richText();

    // login and signup
    $('input[type="radio"][name="signup"]').click(function () {
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".login").not(targetBox).hide();
        $(targetBox).show();
    });

    // popover close outside
    $('.month-select').select2();
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    // date picker
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY',
            debug: true
        });
        $('#datetimepicker2').datetimepicker({
            format: 'DD/MM/YYYY',
            debug: true
        });
    });
});

// deliverable toggle use on the create project pages
$(document).ready(function () {

    $("#deliverable").click(function () {
        $("#filterContent").toggle();
        $(this).toggleClass("filter-box__deliverable-btn-down-active")
    })

    $("#deliverabletwo").click(function () {    
        $("#filterContenttwo").toggle();
        $(this).toggleClass("filter-box__deliverable-btn-down-active")
    })

    $("#deliverablethree").click(function () {
        $("#filterContentthree").toggle();
        $(this).toggleClass("filter-box__deliverable-btn-down-active")
    })
});

// plyr video player
plyr.setup("#plyr-vimeo-example");

// navbar start
function closeNav() {
    document.getElementById("myNav").style.height = "0%";
    var element = document.getElementById("site-logo");
    element.classList.remove("active");
}

function openNav() {
    document.getElementById("myNav").style.height = "100%";
    var element = document.getElementById("site-logo");
    element.classList.add("active");
}
// navbar end

// header backgorundshdow on scroll
$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $(".header").addClass("header-shadow");

    } else {
        $(".header").removeClass("header-shadow");
    }
});

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $(".header--application-nav").addClass("header--application-nav-shadow");

    } else {
        $(".header--application-nav").removeClass("header--application-nav-shadow");
    }
});

// gird and lists view start

// Get the elements with class="column"
var elements = document.getElementsByClassName("common-card--style-2");

// Declare a loop variable
var i;

// List View Grid / List view of courses
function listView() {
    for (i = 0; i < elements.length; i++) {
        elements[i].style.width = "100%";
        elements[i].style.marginLeft = "0%";
    }
    $(".common-card--style-2").addClass("course-card-lists");
}


// Grid View
function gridView() {
    $(".common-card--style-2").removeClass("course-card-lists");
    if ($(window).width() <= 991) {
        for (i = 0; i < elements.length; i++) {
            elements[i].style.width = "48%";
            elements[i].style.marginLeft = "auto ";
        }
    } else {
        for (i = 0; i < elements.length; i++) {
            elements[i].style.width = "31.61%";
            elements[i].style.marginLeft = "2.58% ";
        }
    }
    if ($(".common-card--style-2").hasClass("course-card-lists")) {
        $(this).removeClass("course-card-lists")
    }

}

/* Optional: Add active class to the current button (highlight it) */
var container = document.getElementById("btnContainer");
var btns = container.getElementsByClassName("align-middle");
for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
        var current = document.getElementsByClassName("active-btn");
        current[0].className = current[0].className.replace(" active-btn", "");
        this.className += " active-btn";
    });
}

// required
$(window).resize(function () {
    if ($(window).width() <= 767) {
        $(".common-card--style-2").removeClass("course-card-lists");

        function gridView() {
            for (i = 0; i < elements.length; i++) {
                elements[i].style.width = "100%";
                elements[i].style.marginLeft = "auto ";
            }
        }
    }
});
// grid and lists view end

// replay code on teamchat page
var button = document.getElementById('buttonw');

button.onclick = function () {
    var div = document.getElementById('newpost');
    if (div.style.display !== 'block') {
        div.style.display = 'block';
    } else {
        div.style.display = 'none';
    }
};

// input password js
function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

// fancy
$('[data-fancybox]').fancybox({
    protect: true,
    buttons: [
        'zoom',
        'thumbs',
        'close'
    ]
});